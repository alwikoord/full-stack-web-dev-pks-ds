<?php

class Hewan{
    public $nama;
    public function __construct($nama)
    {
        $this->nama = $nama;
    }
}

class Komodo extends Hewan{
    public function getNama() : string
    {
        return "aku adalah $this->nama";
    }
}

$komodo = new Komodo ('komodo');
echo $komodo->getNama();
?>