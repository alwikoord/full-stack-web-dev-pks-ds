<?php
// Parent class
abstract class Hewan {
  public $name;
  public $darah =50;
  public $jumlahkaki;
  public $keahlian;

  public function __construct($name, $jumlahkaki, $keahlian) {
    $this->name = $name;
    $this->jumlahkaki = $jumlahkaki;
    $this->keahlian = $keahlian;
  }
  abstract public function hewan();
}

abstract class Fight {
  public $attack_power;
  public $defence_power;

  public function __construct($attack_power, $defence_power) {
    $this->attack_power = $attack_power;
    $this->defence_power = $defence_power;
  }
  abstract public function fight();
}

// Child classes
class elang extends hewan {
  public function hewan() : string {
    return "Perkenalkan aku adalah $this->name memiliki jumlah kaki $this->jumlahkaki dan keahlian $this->keahlian";
  }
}

class harimau extends hewan {
  public function hewan() : string {
    return "Perkenalkan aku adalah $this->name memiliki  jumlah kaki $this->jumlahkaki dan keahlian $this->keahlian";
  }
}


// Create objects from the child classes
$elang = new elang("elang", "2", "Terbang Tinggi");
echo $elang->hewan();
echo "<br>";

$harimau = new harimau("harimau", "4", "Lari Cepat");
echo $harimau->hewan();
