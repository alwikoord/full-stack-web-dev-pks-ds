<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email'   => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = Users::where('email', $request->email)->first();

        if($users->otp_code){

            $users->otp_code()->delete();
        }

        
    
        do {
            $random = mt_rand( 100000 , 999999);
            $check = otpcode::where('otp', $random)->first();

        } while ($check);

        $now = Carbon::now();

         $otp_code = otpcode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'users_id' => $users->id
         ]);

         //email


         return response()->json([
                'success' => true,
                'message' => 'otp code berhasil',
                'data' => [
                    'users' => $users,
                    'otp_code' => $otp_code
                ]
             ]);

    }
}
