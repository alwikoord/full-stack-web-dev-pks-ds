<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'otp'   => 'required'
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp', $request->otp)->first();

        if(!$otp_code)
        {
            return response()->json([
                'success' => false,
                'message' => 'otp tidak ada'
            ], 400);

        }

        $now = Carbon::now();

        if($now>$otp_code->valid_until)
        {
            return response()->json([
                'success' => false,
                'message' => 'otp tidak berlaku'
            ], 400);
        }

        $users = Users::find($otp_code->users_id);
        $users->update([
            'email_verified_at' => $now
        ]);

        $otp_code->delete();

        return response()->json([
            'success' => true,
            'message' => 'otp ready banget',
            'data' => $users
        ]);
    }
}
