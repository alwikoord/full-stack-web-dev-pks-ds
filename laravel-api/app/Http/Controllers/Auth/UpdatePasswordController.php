<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email'   => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $users = Users::where('email', $request->email)->first();

        if(!$users){
            return response()->json([
                'success' => false,
                'message' => 'email tidak ada bro..'
            ], 400);
        }
        $users->update([
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'password berhasil diubah bro...',
            'data' => $users
        ]);
    }
}
