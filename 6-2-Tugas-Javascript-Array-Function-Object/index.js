// Jawaban Soal Nomer 1
var i=0, item, items = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
items.sort()
while(item = items[i++]){
    console.log(item);
}

//Jawaban Soal Nomer 2
var data = [{    
    name: "John",
    age: 30,
    address: "Jalan Pelesiran",
    hobby : "Gaming"
}]

data.forEach(function(item){
    console.log("Nama saya " + item.name + ", umur saya " + item.age + " tahun, " + "alamat saya di " + item.address + ", dan saya punya hobby yaitu " + item.hobby)
})

//Jawaban Soal Nomer 3
var hitung_1 = "Muhammad"
var hitung_2 = "Iqbal"

function countVowel(str) { 

    const count = str.match(/[aeiou]/gi).length;
    return count;
}

const result1 = countVowel(hitung_1);
const result2 = countVowel(hitung_2);

console.log("Jumlah Huruf Vokal Adalah : " + result1);
console.log("Jumlah Huruf Vokal Adalah : " + result2);

//Jawaban Soal Nomer 4
var hitung = [-2, 0, 2, 4, 7, 8]


console.log( hitung[0] ) // -2
console.log( hitung[1] ) // 0
console.log( hitung[2] ) // 2
console.log( hitung[3] ) // 4
console.log( hitung[5] ) // 8